def slugify(value):
    import re

    value = re.sub(r"[^\w\s-]", "", value).strip().lower()
    value = re.sub(r"[-\s]+", "-", value)

    return value


def clean_url(value):
    value = str(value).replace("\\n", "").replace('"', "")

    return value
