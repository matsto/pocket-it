dated to 11/04/2020

# Read-me
## Description
Simple console app that uses Pocket API to get all content from the users account and either save it locally in txt files or using the google tts save them as mp3 files.

The application uses asyncio and multithreading to improve the efficiency.

## Installation
The installation is as easy as typing `pip install -r requirements.txt`

## Launch
To run it, launch `python main.py` and friendly and straight-forward menu will appear.

