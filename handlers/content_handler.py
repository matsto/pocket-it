import asyncio
import os
import re

import aiohttp
import pandas as pd
from bs4 import BeautifulSoup

from handlers.error_handler import error_handler
from handlers.file_handler import FileHandler
from utils.cleaners import clean_url, slugify


async def fetch(session, url):
    async with session.get(url) as response:
        return await response.text()


async def main(link, index):
    async with aiohttp.ClientSession() as session:
        html = await fetch(session, link)
        print("opening " + link)
        soup = BeautifulSoup(html, "html.parser")

        try:
            soup_bodytext = soup.body.get_text()
        except AttributeError as error:
            error_handler(link, error.args)
            return
        soup_bodytext = re.sub(r"[\s\_]", " ", soup_bodytext)
        soup_title = soup_bodytext[0:50]

        if len(soup_bodytext) < 200:
            problem = "ShortContentError"
            error_handler(link, problem)
            return

        newpath = r"./Output/txt/"
        if not os.path.exists(newpath):
            os.makedirs(newpath)

        title = f"{index}. {slugify(soup_title)}"
        text_file = FileHandler(f"./Output/txt/{title}.txt")
        text_file.write(soup_bodytext)


def read_and_download():
    # read content of output.csv and download the content (as text)
    df = pd.read_csv("./Output/Output.csv", encoding="utf-8")

    lines = df["resolved_url"].tolist()
    tasks = []
    loop = asyncio.new_event_loop()
    for index, line in enumerate(lines):
        line = clean_url(line)
        tasks.append(loop.create_task(main(line, index + 1)))
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
