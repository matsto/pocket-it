from handlers.file_handler import FileHandler


def error_handler(link, problem) -> None:
    # function to handle an error in clear and easy way

    file_handler = FileHandler("./Output/Errors.txt")
    file_handler.add(f"{problem} {link}")

    return
