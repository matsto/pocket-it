import os
import re
from queue import Queue
from threading import Thread

import pandas as pd
from gtts import gTTS

from handlers.file_handler import FileHandler


class gttsThread(Thread):
    def __init__(self, queue):
        super().__init__()
        self.queue = queue

    def run(self):
        while True:
            text, article_lang, audio_file_name = self.queue.get()
            print(f"saving: {audio_file_name}")
            tts = gTTS(text, lang=article_lang)

            try:
                tts.save(f"./Output/mp3/{audio_file_name}")
            finally:
                self.queue.task_done()


def read_and_say():
    # read every text file and create mp3 using gTTS

    mypath = r"./Output/txt/"
    files = [
        f
        for f in os.listdir(mypath)
        if os.path.isfile(os.path.join(mypath, f))
    ]
    queue = Queue()
    newpath = r"./Output/mp3"
    if not os.path.exists(newpath):
        os.makedirs(newpath)
    for x in range(20):
        worker = gttsThread(queue)
        worker.daemon = True
        worker.start()
    df = pd.read_csv("./Output/Output.csv", encoding="utf-8")

    lang_dict = df["lang"].to_dict()
    pattern = r"^([\d]+\.)"

    for file_name in files:
        match = re.search(pattern, file_name)

        if match:
            index = int(match.group().replace(".", ""))
        else:
            continue

        article_lang = lang_dict[index]

        if not isinstance(article_lang, str):
            continue

        text_file = FileHandler(f"./Output/txt/{file_name}")
        text = text_file.read()

        audio_file_name = file_name.replace(".txt", f"_{article_lang}.mp3")

        queue.put((text, article_lang, audio_file_name))
    queue.join()
