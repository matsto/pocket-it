import os
import webbrowser

import pandas as pd
import requests

from handlers.file_handler import FileHandler
from settings.base import CONSUMER_KEY


class PocketHandler:
    def __init__(self) -> None:
        self.file_handler = FileHandler()

    def get_request_token(self):
        url = "https://getpocket.com/v3/oauth/request"
        payload = {
            "consumer_key": CONSUMER_KEY,
            "redirect_uri": "http://www.google.com",
        }

        response = requests.post(url, payload)
        result = response.text.replace("code=", "")

        self.file_handler.write(f"REQUEST_TOKEN={result}\n")
        self.request_token = result

    def aprove_app(self):
        webbrowser.open(
            f"https://getpocket.com/auth/authorize?request_token={self.request_token}&redirect_uri=http://www.google.com",
            new=2,
            autoraise=True,
        )

    def get_access_token(self):
        payload = {"consumer_key": CONSUMER_KEY, "code": self.request_token}
        url = "https://getpocket.com/v3/oauth/authorize"

        response = requests.post(url, payload)
        result = response.text.split("&")

        self.access_token = result[0].replace("access_token=", "")
        self.file_handler.write(f"ACCESS_TOKEN={self.access_token}")

    def get_data(self):
        if hasattr(self, "access_token"):
            access_token = self.access_token
        else:
            access_token = self.file_handler.read().replace(
                "ACCESS_TOKEN=", ""
            )

        parameters = {
            "consumer_key": CONSUMER_KEY,
            "access_token": access_token,
        }

        response = requests.get(
            "https://getpocket.com/v3/get", params=parameters
        )
        articles = response.json()["list"]

        df = pd.DataFrame(articles)
        df = df.swapaxes(0, 1)

        df = df.filter(items=["lang", "resolved_url"], axis="columns")

        df_csv = df.to_csv(index=False)

        output_folder = r"./Output"
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        output_file_handler = FileHandler("./Output/Output.csv")
        output_file_handler.write(df_csv)
