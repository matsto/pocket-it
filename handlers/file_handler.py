class FileHandler:
    def __init__(
        self, file_name: str = "LOCAL_TOKENS",
    ):
        self.file_name = file_name

    def write(self, text: str) -> None:
        with open(self.file_name, "w") as f:
            f.write(text)
        return

    def read(self) -> str:
        with open(self.file_name, "r") as f:
            text = f.read()
        return text

    def add(self, text: str) -> None:
        with open(self.file_name, "a") as f:
            f.write(text)
        return
