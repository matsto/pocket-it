import sys
from os import system

from handlers import content_handler
from handlers.file_handler import FileHandler
from handlers import gtts_handler
from handlers.pocket_handler import PocketHandler


def info():
    menu_file = FileHandler(f"./static/menu.txt")
    menu = menu_file.read()
    print(menu)


pocket_handler = PocketHandler()


def menu(choose):
    for item in choose:
        if item == "1":
            pocket_handler.get_request_token()
            pocket_handler.aprove_app()
            pocket_handler.get_access_token()

        elif item == "2":
            pocket_handler.get_data()

        elif item == "3":
            content_handler.read_and_download()

        elif item == "4":
            gtts_handler.read_and_say()

        elif item == "q" or "Q":
            sys.exit(system("clear"))

        else:
            print("that value is unknown")


if __name__ == "__main__":
    system("clear")
    info()

    while 1 == 1:
        menu(input("select option from menu: "))
